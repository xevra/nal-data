Citations:

3D fits: 
@misc{https://doi.org/10.48550/arxiv.2107.13082,
  doi = {10.48550/ARXIV.2107.13082},
  url = {https://arxiv.org/abs/2107.13082},
  author = {Delfavero, Vera and O'Shaughnessy, Richard and Wysocki, Daniel and Yelikar, Anjali},
  keywords = {General Relativity and Quantum Cosmology (gr-qc), FOS: Physical sciences, FOS: Physical sciences},
  title = {Normal Approximate Likelihoods to Gravitational Wave Events},
  publisher = {arXiv},
  year = {2021},
  copyright = {arXiv.org perpetual, non-exclusive license}
}

Higher dimensional fits:
@misc{https://doi.org/10.48550/arxiv.2205.14154,
  doi = {10.48550/ARXIV.2205.14154},
  url = {https://arxiv.org/abs/2205.14154},
  author = {Delfavero, Vera and O'Shaughnessy, Richard and Wysocki, Daniel and Yelikar, Anjali},
  keywords = {Instrumentation and Methods for Astrophysics (astro-ph.IM), General Relativity and Quantum Cosmology (gr-qc), FOS: Physical sciences, FOS: Physical sciences},
  title = {Compressed Parametric and Non-Parametric Approximations to the Gravitational Wave Likelihood},
  publisher = {arXiv},
  year = {2022},
  copyright = {arXiv.org perpetual, non-exclusive license}
}

Usage:

# Clone repository with Gravitational Wave gaussian fits
git clone https://gitlab.com/xevra/nal-data.git

# Read file structure
#[CATALOG].nal.hdf5
#|__ group: [EVENT]
#    |__ group: [LABEL]
#        |__attrs
#        |__dset: mean
#        |__dset: cov

# CATALOG:
# GWTC-1, etc...

# EVENT:
# GW events:
# Ex: GW150914
# Ex: GW190408_181802

# LABELS:
# [coord_tag]:[group]:[fit_method]
# Ex: aligned3d_source:IMRPhenomPv2:simple
#   This is a trivial fit, inferred from the mean and covariance of the catalog
# Ex: aligned3d_source:IMRPhenomPv2:select
#   This is the fit properly optimized

# Mean
# float (ndim)
# mean value for each parameter

# Cov
# float (ndim x ndim)
# Covariance matrix for fit

# std = sqrt(diagonal(cov))
# std would have same units as mean

# Cor
# Correlation matrix associated with Cov

# Limits
# Bounded region of fit

# Scale
# Scale used to protect from eigenvalue issues for coordinates with different
# sizes

# ATTRIBUTES:
#
# coord_tag: coordinate reference label
# coords: list of coordinate names, including prior
# fit_method: method for generating parameter guesses
# fit_time: time to run fit (seconds
# group: the waveform approximate samples are provided by in the catalog
# kl: The RMS value of the kl divergences for each dimension. This is the goodness of fit measurement reported in our publications
# nstep: number of proposed steps for random walkers
# nwalk: number of random walkers used for fit
