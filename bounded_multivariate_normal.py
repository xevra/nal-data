#!/usr/bin/env python3
'''Demonstrate how to load and understand data

Full code release coming along with paper in 2022.

In the meantime, this is how you can load the fits
'''

######## Imports ########

import h5py
import numpy as np
from os.path import isfile, isdir, join
from scipy.stats import multivariate_normal

######## MultivariateNormal Class ########

class MultivariateNormal(object):
    '''Load and use nal models'''

    def __init__(
                 self,
                 mu,
                 Sig,
                 scale,
                 limits,
                 sig,
                 cor,
                 attrs=None,
                ):
        '''
        Parameters
        ----------
        mu: array like, shape = (ndim)
            Input $\mathbf{\mu}$ params for NAL model
        Sig: array like, shape = (ndim, ndim)
            Input $\mathbf{\Sigma}$ for NAL model
        scale: array like, shape = (ndim)
            Input re-scale factor, for eigenvalue decomposition
        limits: array like, shape = (ndim, 2)
            Bounded region for model
        sig: array like, shape = (ndim)
            Input: redundant sigma parameters for NAL model
        cor: array like, shape = (ndim, ndim)
            Input: redundant correlation matrix parameters for NAL model
        attrs: dict, optional
            Provides attributes associated with fit
        '''
        # Store inputs
        self.mu     = mu
        self.Sig    = Sig
        self.scale  = scale
        self.limits = limits
        self.attrs  = attrs
        self.sig    =  sig
        self.cor    = cor
        # Dimensionality
        self.ndim = mu.size
        # Calculate scaled quantities
        self._mu_scaled = mu/scale
        self._Sig_scaled = Sig / np.outer(scale,scale)
        self._limits_scaled = limits/scale[:,None]


    @staticmethod
    def load(
             fname_nal,
             event,
             label,
            ):
        '''Load parameters for a particular event

        Parameters
        ----------
        fname_nal: str
            Input path to GWTC-X.nal.hdf5 file
        event: str
            Input The name of the event in the GWTC catalog (ex: GW150914)
        label: str
            Input model identifier for a given event
        '''
        # Check if release file exists
        if not isfile(fname_nal):
            raise RuntimeError("No such file: %s"%fname_nal)
        # Initialize file object
        with h5py.File(fname_nal, 'r') as F:
            # Check if event exists
            if not (event in F):
                # Check for similar events
                events = []
                for item in F:
                    if item.startswith(event[:8]):
                        raise RuntimeError(
                            "No event %s in %s\n"%(event, fname_nal),
                            "Did you mean %s?"%(item)
                            )
                raise RuntimeError(
                    "No event %s in %s\n"%(event, fname_nal),
                    )

            # Initialize event group
            event_obj = F[event]

            # Initialize label
            coord_tag, group, fit_method = label.split(':')
            #label = "%s:%s:%s"%(coord_tag,group,fit_method)
        
            # Check that label exists
            if not (label in event_obj):
            
                # Initialize some information
                coord_tags = []
                groups = []
                fit_methods = []

                # Loop through labels in GWTC-X.nal.hdf5
                for item in event_obj:

                    # Split up labels
                    item_coord_tag, item_group, item_fit_method = item.split(":")
                    # Append to list
                    if not item_coord_tag in coord_tags:
                        coord_tags.append(item_coord_tag)
                    if (not item_group in groups) and (coord_tag == item_coord_tag):
                        groups.append(item_group)
                    if (not item_fit_method in fit_methods) and \
                        (coord_tag == item_coord_tag) and \
                        (group == item_group):
                        fit_methods.append(item_fit_method)

                # check things for user before raising exception
                if not coord_tag in coord_tags:
                    raise RuntimeError(
                        "Invalid coord_tag: %s\n"%coord_tag,
                        "Available coord tags: ", coord_tags
                       )
                if not group in groups:
                    raise RuntimeError(
                        "Unavailable group %s for %s %s"%(group, event, coord_tag),
                        "Available groups: ", groups
                       )
                if not fit_method in fit_methods:
                    raise RuntimeError(
                        "Unavailable fit method %s for %s %s %s"%(
                            fit_method, event, coord_tag, group),
                        "Available fit methods: ", fit_methods
                       )
                # We haven't caught the reason for the exception, but we still
                # need one
                raise RuntimeError("Unavailable label: %s"%label)

            # Initialize label group
            label_obj = event_obj[label]

            # Initialize dictionary for event
            event_dict = {"event":event}
            event_attrs = {}

            # Loop datasets
            for dset in label_obj:
                event_dict[dset] = label_obj[dset][...]

            # Loop attrs
            for attr in label_obj.attrs:
                event_attrs[attr] = label_obj.attrs[attr]

            # Initialize object
            return MultivariateNormal(
                                      event_dict["mean"],
                                      event_dict["cov"],
                                      event_dict["scale"],
                                      event_dict["limits"],
                                      event_dict["std_dev"],
                                      event_dict["cor"],
                                      attrs=event_attrs,
                                     )

    def sample(self,n_sample=10, seed=0):
        '''Draw samples from your gaussian in your limited space
        
        Parameters
        ----------
        n_sample: int, optional
            Input number of samples to draw
        seed: int, optional
            Integer seed for sample generation
        '''
        # initialize samples
        samples = np.empty((n_sample, self.mu.size))
        # Initialize loop variable
        n_keep = 0
        # Begin while loop
        while n_keep < n_sample:
            # Draw samples from scaled distribution
            # _values are not physcally scaled, and are therefore hidden
            _raw_samples = multivariate_normal.rvs(
                                                   self._mu_scaled,
                                                   self._Sig_scaled,
                                                   n_sample - n_keep,
                                                   random_state=seed,
                                                  )
            # Ensure 2d
            _raw_samples = np.atleast_2d(_raw_samples)
            # Determine keep
            keep = np.ones(n_sample-n_keep,dtype=bool)
            # loop dimensions
            for i in range(self.ndim):
                keep = keep & \
                        (_raw_samples[:,i] > self._limits_scaled[i][0]) & \
                        (_raw_samples[:,i] < self._limits_scaled[i][1])
            # check keep
            if np.sum(keep) > 0:
                # Update samples
                samples[n_keep:n_keep + np.sum(keep)] = _raw_samples[keep]
                # Update keep
                n_keep += int(np.sum(keep))

        # Rescale samples
        samples *= self.scale

        # Return samples
        return samples

    def likelihood(self, samples):
        '''Evaluate the likelihood on some samples

        Parameters
        ----------
        samples: array like, (npts, ndim)
            Input samples for evaluation
        '''
        # Scale samples
        _scaled_samples = samples/self.scale

        # Initialize pdf
        pdf = np.zeros((samples.shape[0]))

        # Check bounds
        bounded = np.ones(samples.shape[0],dtype=bool)
        # loop dimensions
        for i in range(self.ndim):
            bounded = bounded & \
                        (samples[:,i] > self.limits[i][0]) & \
                        (samples[:,i] < self.limits[i][1])
        # Set out of bounds to zero
        pdf[~bounded] = 0.0

        # Evaluate the remaining samples
        pdf[bounded] = multivariate_normal.pdf(
                                               _scaled_samples[bounded],
                                               self._mu_scaled,
                                               self._Sig_scaled,
                                              )
        # Note: the version of the likelihood function which 
        # comes with the gwalk repo will be much faster than this
        return pdf


    def save(
             self,
             fname_nal,
             event,
             label,
             compression="gzip",
            ):
        ''' Save a fit to a file somewhere

        Parameters
        ----------
        fname_nal: str
            Input path to GWTC-X.nal.hdf5 file
        event: str
            Input The name of the event in the GWTC catalog (ex: GW150914)
        label: str
            Input model identifier for a given event
        compression: str, optional
            Input what kind of compression should we save fits with?
        '''
        # Ensure database exists
        with h5py.File(fname_nal, 'a') as F:
            pass
        # Initialize file object
        with h5py.File(fname_nal, 'r+') as F:
            # Check if the group exists
            if not event in F:
                F.create_group(event)
            # Load the event object
            event_obj = F[event]
            # Check if the label exists
            if not label in event_obj:
                event_obj.create_group(label)
            # Load label object
            label_obj = event_obj[label]
            # Initialize datasets
            if not "mean" in label_obj:
                label_obj.create_dataset(
                        "mean", self.mu.shape, compression=compression, dtype=self.mu.dtype)
            label_obj["mean"][...] = self.mu
            if not "cov" in label_obj:
                label_obj.create_dataset(
                        "cov", self.Sig.shape, compression=compression, dtype=self.Sig.dtype)
            label_obj["cov"][...] = self.Sig
            if not "scale" in label_obj:
                label_obj.create_dataset(
                        "scale", self.scale.shape, compression=compression, dtype=self.scale.dtype)
            label_obj["scale"][...] = self.scale
            if not "limits" in label_obj:
                label_obj.create_dataset(
                        "limits", self.limits.shape, compression=compression, dtype=self.limits.dtype)
            label_obj["limits"][...] = self.limits
            if not "std_dev" in label_obj:
                label_obj.create_dataset(
                        "std_dev", self.sig.shape, compression=compression, dtype=self.sig.dtype)
            label_obj["std_dev"][...] = self.sig
            if not "cor" in label_obj:
                label_obj.create_dataset(
                        "cor", self.cor.shape, compression=compression, dtype=self.cor.dtype)
            label_obj["cor"][...] = self.cor

            # Set attributes
            for item in self.attrs:
                label_obj.attrs[item] = self.attrs[item]

    def update_limits(self, new_limits):
        '''Set new limits for the model
        
        Parameters
        ----------
        new_limits: array like, shape = (ndim, 2)
            Input new limits
        '''
        self.limits = new_limits
        self._limits_scaled = new_limits/scale[:,None]

    def normalize():
        return

######## Main for example ########
def main():
    fname_nal = "GWTC-1.nal.hdf5"
    event = "GW150914"
    label = "aligned3d_source:IMRPhenomPv2:select"
    mynal = MultivariateNormal.load(
                                   fname_nal,
                                   event,
                                   label,
                                   )
    x = mynal.sample()

    L1 = mynal.likelihood(x)
    mynal.save("foo.nal.hdf5", event, label)
    altnal = MultivariateNormal.load("foo.nal.hdf5", event, label)
    L2 = altnal.likelihood(x)
    print(np.sum(L2-L1))

######## Execution ########
if __name__ == "__main__":
    main()

            


            





